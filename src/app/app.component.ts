import { AuthService } from './auth/auth.service';
import { HttpService } from './services/http.service';
import { Component } from '@angular/core';
import { TasksService } from './services/tasks.service';
import { Router } from '../../node_modules/@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(public authService: AuthService, private router: Router) {

  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/']);
  }
}
