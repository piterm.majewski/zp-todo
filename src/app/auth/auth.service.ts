import { Injectable } from '@angular/core';
import { AngularFireAuth } from '../../../node_modules/angularfire2/auth';
import { User } from '../../../node_modules/firebase';
import { Router } from '../../../node_modules/@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: User;

  constructor(private anguarFireAuth: AngularFireAuth, private router: Router) {
    anguarFireAuth.authState.subscribe(user => {
      this.user = user;
    });
  }

  login(email: string, password: string) {
    this.anguarFireAuth.auth.signInWithEmailAndPassword(email, password).then(user => {
      this.router.navigate(['/todoTask']);
    }).catch(value => {
      console.log(value);
    });
  }

  signup(email: string, password: string) {
    this.anguarFireAuth.auth.createUserWithEmailAndPassword(email, password).then(user => {
      console.log(user);
    }).catch(value => {
      console.log(value);
    });
  }

  logout() {
    this.anguarFireAuth.auth.signOut();
  }
}
