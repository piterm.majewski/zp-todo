import { Component, OnInit, Input } from '@angular/core';
import { TasksService } from '../services/tasks.service';
import { Task } from '../models/Task';

@Component({
  selector: 'app-done-task',
  templateUrl: './done-task.component.html',
  styleUrls: ['./done-task.component.css']
})
export class DoneTaskComponent implements OnInit {

  taskDone: Array<Task> = [];

  constructor(private tasksService: TasksService) {
    tasksService.getTasksListObs().subscribe((i: Array<Task>) => {
      // slice jest dodane aby pipe sortName wiedział że zminiła się referencja i należy na nowo go przez niego przepucić
      this.taskDone = i.filter(task => task.isDone === true);
    });
  }

  ngOnInit() {
  }
}
