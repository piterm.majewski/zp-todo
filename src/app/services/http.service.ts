import { AuthService } from './../auth/auth.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Task } from '../models/Task';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  readonly DB_URL = '/api/tasks';
  private param: HttpParams;

  constructor(private httpClient: HttpClient, private authService: AuthService) {
    this.param = new HttpParams();
    this.getTasks();
  }

  getTasks(): Observable<Array<Task>> {
    return this.httpClient.get<Array<Task>>(this.DB_URL, {params: this.getParams()});
  }

  getParams() {
    this.param.set('userId', this.authService.user.uid);
    return this.param;
  }

  saveTask(task: Task): Observable<Task> {
    return this.httpClient.post<Task>(this.DB_URL, task);
  }

  saveTasks(tasks: Array<Task>): Observable<Array<Task>> {
    console.log('HttpService -> saveTasks()', tasks);
    return this.httpClient.post<Array<Task>>(this.DB_URL, tasks);
  }

  removeTask(taskId: string): Observable<void> {
    return this.httpClient.delete<void>(this.DB_URL + '/' + taskId);
  }
}
