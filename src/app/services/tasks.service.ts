import { HttpService } from './http.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Task } from '../models/Task';
import { AuthService } from '../auth/auth.service';
import { AngularFireAuth } from '../../../node_modules/angularfire2/auth';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  private tasksListObs = new BehaviorSubject<Array<Task>>([]);

  constructor(private httpService: HttpService, private angularFire: AngularFireAuth) {
    angularFire.authState.subscribe(user => {
      if (user) {
        this.init();
      } else {
        this.tasksListObs.next([]);
      }
    });
  }

  init() {
    const taskList = this.httpService.getTasks().subscribe((tasks: Array<Task>) => {
      this.tasksListObs.next(tasks);
    });
  }

  add(task: Task) {
    const taskList = this.tasksListObs.getValue();
    taskList.push(task);
    this.tasksListObs.next(taskList);
  }

  remove(task: Task) {
    if (task.id !== null) {
      this.httpService.removeTask(task.id).subscribe();
    }
    const taskList = this.tasksListObs.getValue().filter(i => i !== task);
    this.tasksListObs.next(taskList);
  }

  done(task: Task) {
    task.end = new Date().toLocaleDateString();
    task.isDone = true;
    const taskList = this.tasksListObs.getValue();
    this.tasksListObs.next(taskList);
  }

  getTasksListObs(): Observable<Array<Task>> {
    return this.tasksListObs.asObservable();
  }

  saveToDB() {
    console.log('TaskService -> saveToDB()');
    this.httpService.saveTasks(this.tasksListObs.getValue()).subscribe(
      tasks => {
        console.log('result: ', tasks);
      }, result => {
        console.log('Error: ', result);
      }
   );
  }
}
