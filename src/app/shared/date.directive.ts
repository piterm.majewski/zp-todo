 import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';
import { renderComponent } from '@angular/core/src/render3';

@Directive({
  selector: '[appDate]'
})
export class DateDirective {

  @Input()
  private date: Date;
  private paragraph;

  constructor(private el: ElementRef, private renderer: Renderer2) {
    this.paragraph = this.renderer.createElement('p');
  }

  @HostListener('mouseenter')
  mouseEnter(eventDate: Event) {
    this.paragraph.innerHTML = '<div class=\"col-sm-12\">' + this.date.toLocaleString() + '</div>';
    this.renderer.appendChild(this.el.nativeElement, this.paragraph);
  }


  @HostListener('mouseleave')
  mouseLeave(eventDate: Event) {
    this.renderer.removeChild(this.el.nativeElement, this.paragraph);
    console.log(this.date);
  }
}
